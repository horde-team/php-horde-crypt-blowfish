php-horde-crypt-blowfish (1.1.4-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

  [ Mike Gabriel ]
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.
  * d/patches: Add 1012_php8.2.patch. Fix PHP 8.2 deprecation warnings.
    (Closes: #1028979).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 25 Jan 2023 08:10:50 +0100

php-horde-crypt-blowfish (1.1.4-1) unstable; urgency=medium

  * New upstream version 1.1.4
  * d/patches: Adapt path names in patch files to new upstream version.
  * d/watch: Switch to format version 4.
  * d/control: Bump Standards-Version: to 4.6.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 13 Jul 2022 22:04:58 +0200

php-horde-crypt-blowfish (1.1.3-2) unstable; urgency=medium

  * d/patches: Add debian/patches/1010_phpunit-8.x+9.x.patch.
    Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Jul 2020 17:36:04 +0000

php-horde-crypt-blowfish (1.1.3-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * New upstream version 1.1.3
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:31:41 +0200

php-horde-crypt-blowfish (1.1.2-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959250).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:16:59 +0200

php-horde-crypt-blowfish (1.1.2-4) unstable; urgency=medium

  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 18:51:13 +0200

php-horde-crypt-blowfish (1.1.2-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 00:01:58 +0200

php-horde-crypt-blowfish (1.1.2-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 11:20:16 +0200

php-horde-crypt-blowfish (1.1.2-1) unstable; urgency=medium

  * New upstream version 1.1.2

 -- Mathieu Parent <sathieu@debian.org>  Fri, 09 Sep 2016 12:00:58 +0200

php-horde-crypt-blowfish (1.1.1-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Tue, 07 Jun 2016 22:15:24 +0200

php-horde-crypt-blowfish (1.1.1-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Mar 2016 22:37:47 +0100

php-horde-crypt-blowfish (1.1.1-1) unstable; urgency=medium

  * New upstream version 1.1.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 14:04:45 +0100

php-horde-crypt-blowfish (1.1.0-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 15:13:24 +0200

php-horde-crypt-blowfish (1.1.0-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 22:18:49 +0200

php-horde-crypt-blowfish (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 16 Jun 2015 13:35:40 +0200

php-horde-crypt-blowfish (1.0.3-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 1.0.3

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 20:14:23 +0200

php-horde-crypt-blowfish (1.0.2-6) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 11:59:52 +0200

php-horde-crypt-blowfish (1.0.2-5) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 10:08:14 +0200

php-horde-crypt-blowfish (1.0.2-4) unstable; urgency=medium

  * Fix DEP-8 depends

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:36:42 +0200

php-horde-crypt-blowfish (1.0.2-3) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 24 Aug 2014 09:24:05 +0200

php-horde-crypt-blowfish (1.0.2-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 18:54:19 +0200

php-horde-crypt-blowfish (1.0.2-1) unstable; urgency=low

  * New upstream version 1.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 20:17:34 +0100

php-horde-crypt-blowfish (1.0.1-4) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:36:36 +0100

php-horde-crypt-blowfish (1.0.1-3) unstable; urgency=low

  * Needs pkg-php-tools >= 0.9 or build will fail

 -- Mathieu Parent <sathieu@debian.org>  Fri, 04 Jan 2013 19:48:10 +0100

php-horde-crypt-blowfish (1.0.1-2) unstable; urgency=low

  * Fix Vcs-Browser and Horde dir

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Dec 2012 15:23:10 +0100

php-horde-crypt-blowfish (1.0.1-1) unstable; urgency=low

  * Horde_Crypt_Blowfish package
  * Initial packaging (Closes: #694752)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 21:31:25 +0100
